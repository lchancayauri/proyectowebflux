package com.operaciones.tipocambio.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;

@Data
@Table("tipocambio")
public class TipoCambio {

	@Id
	private Integer id;
	@Column("monedaOrigen")
	private String monedaOrigen;
	@Column("monedaDestino")
	private String monedaDestino;
	@Column("tipoCambio")
	private Double tipoCambio;

	public TipoCambio(Integer id, String monedaOrigen, String monedaDestino, Double tipoCambio) {
		super();
		this.id = id;
		this.monedaOrigen = monedaOrigen;
		this.monedaDestino = monedaDestino;
		this.tipoCambio = tipoCambio;
	}

	public Integer getId() {
		return id;
	}

	public void setIdTipoCambio(Integer id) {
		this.id = id;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public Double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

}
