package com.operaciones.tipocambio.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;

@Data
@Table("operaciontipocambio")
public class OperacionTipoCambio {

	@Id
	private Integer id;
	@Column("monedaOrigen")
	private String monedaOrigen;
	@Column("monedaDestino")
	private String monedaDestino;
	@Column("tipoCambio")
	private Double tipoCambio;
	@Column("montotipoCambio")
	private Double montoTipoCambio;
	@Column("usuarioauditoria")
	private String usuarioauditoria;

	public OperacionTipoCambio(Integer id, String monedaOrigen, String monedaDestino, Double tipoCambio,
			Double montoTipoCambio) {
		super();
		this.id = id;
		this.monedaOrigen = monedaOrigen;
		this.monedaDestino = monedaDestino;
		this.tipoCambio = tipoCambio;
		this.montoTipoCambio = montoTipoCambio;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public Double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public Double getMontoTipoCambio() {
		return montoTipoCambio;
	}

	public void setMontoTipoCambio(Double montoTipoCambio) {
		this.montoTipoCambio = montoTipoCambio;
	}

	public String getUsuarioauditoria() {
		return usuarioauditoria;
	}

	public void setUsuarioauditoria(String usuarioauditoria) {
		this.usuarioauditoria = usuarioauditoria;
	}

}
