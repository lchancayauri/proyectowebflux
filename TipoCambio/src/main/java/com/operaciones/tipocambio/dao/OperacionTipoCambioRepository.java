package com.operaciones.tipocambio.dao;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.operaciones.tipocambio.model.OperacionTipoCambio;

public interface OperacionTipoCambioRepository extends ReactiveCrudRepository<OperacionTipoCambio, Integer>{

}
