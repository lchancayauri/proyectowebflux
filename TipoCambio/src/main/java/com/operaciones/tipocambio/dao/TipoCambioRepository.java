package com.operaciones.tipocambio.dao;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.operaciones.tipocambio.model.TipoCambio;

public interface TipoCambioRepository extends ReactiveCrudRepository<TipoCambio, Integer>{

}
