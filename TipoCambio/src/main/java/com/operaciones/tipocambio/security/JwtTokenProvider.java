package com.operaciones.tipocambio.security;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.operaciones.tipocambio.exception.TipoCambioAppException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenProvider {

	private static final Logger log = LoggerFactory.getLogger(JwtTokenProvider.class);
	
	@Value("${app.jwt-secret}")
	private String jwtSecret;

	@Value("${app.jwt-expiration-milliseconds}")
	private int jwtExpirationInMs;

	public String generarToken(Authentication authentication) {
		String username = authentication.getName();
		Date fechaActual = new Date();
		Date fechaExpiracion = new Date(fechaActual.getTime() + jwtExpirationInMs);
		
		log.info("generarToken -> username : "+username +", jwtExpirationInMs:"+jwtExpirationInMs +", fechaFinal : "+fechaExpiracion.toGMTString());
		log.info("jwtSecret: "+jwtSecret);
		String token = Jwts.builder().setSubject(username).setIssuedAt(new Date()).setExpiration(fechaExpiracion)
				.signWith(SignatureAlgorithm.HS512, jwtSecret).compact();

		return token;
	}

	public String obtenerUsernameJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		return claims.getSubject();
	}

	public boolean validarToken(String token) {

		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
			return true;
		} catch (SignatureException ex) {
			throw new TipoCambioAppException(HttpStatus.BAD_REQUEST, "Firma JWT no valida");
		} catch (MalformedJwtException ex) {
			throw new TipoCambioAppException(HttpStatus.BAD_REQUEST, "Firma JWT no valida");
		} catch (ExpiredJwtException ex) {
			throw new TipoCambioAppException(HttpStatus.BAD_REQUEST, "Firma JWT caducado");
		} catch (UnsupportedJwtException ex) {
			throw new TipoCambioAppException(HttpStatus.BAD_REQUEST, "Firma JWT no compatible");
		} catch (IllegalArgumentException ex) {
			throw new TipoCambioAppException(HttpStatus.BAD_REQUEST, "La cadena claims JWT esta vacia");
		}
	}
}
