package com.operaciones.tipocambio.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	public InMemoryUserDetailsManager inMemoryUserDetailsManager;

	public CustomUserDetailsService() {
		super();
		// TODO Auto-generated constructor stub
		UserDetails usuario = User.builder().username("operador").password(passwordEncoderService().encode("12345"))
				.roles("USER").build();
		UserDetails admin = User.builder().username("admin").password(passwordEncoderService().encode("admin"))
				.roles("ADMIN").build();
		inMemoryUserDetailsManager = new InMemoryUserDetailsManager(usuario, admin);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UserDetails userDetails = inMemoryUserDetailsManager.loadUserByUsername(username);
		return userDetails;
	}

	@Bean
	PasswordEncoder passwordEncoderService() {
		return new BCryptPasswordEncoder();
	}
}
