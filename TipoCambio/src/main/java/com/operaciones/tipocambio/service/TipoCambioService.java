package com.operaciones.tipocambio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.operaciones.tipocambio.dao.OperacionTipoCambioRepository;
import com.operaciones.tipocambio.dao.TipoCambioRepository;
import com.operaciones.tipocambio.model.OperacionTipoCambio;
import com.operaciones.tipocambio.model.TipoCambio;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TipoCambioService {

	@Autowired
	TipoCambioRepository repositoryTipoCambio;
	
	@Autowired
	OperacionTipoCambioRepository repositoryOperacionTipoCambio;

	public Flux<TipoCambio> getTiposCambio() {
		return repositoryTipoCambio.findAll();
	}

	public Mono<TipoCambio> getTipoCambio(Integer id) {
		return repositoryTipoCambio.findById(id);
	}

	public Mono<TipoCambio> createTipoCambio(TipoCambio tipoCambio) {
		return repositoryTipoCambio.save(tipoCambio);
	}

	public Mono<TipoCambio> updateTipoCambio(TipoCambio tipoCambio, Integer id) {
		return repositoryTipoCambio.findById(id).map((c) -> {
			c.setMonedaOrigen(tipoCambio.getMonedaOrigen());
			c.setMonedaDestino(tipoCambio.getMonedaDestino());
			c.setTipoCambio(tipoCambio.getTipoCambio());
			return c;
		}).flatMap(c -> repositoryTipoCambio.save(c));

	}

	public Mono<Void> deleteTipoCambio(Integer id) {
		return repositoryTipoCambio.deleteById(id);
	}
	
	public Flux<OperacionTipoCambio> getOperacionsTipoCambio() {
		return repositoryOperacionTipoCambio.findAll();
	}
	
	public Mono<OperacionTipoCambio> createOperacionTipoCambio(OperacionTipoCambio operacionTipoCambio) {
		return repositoryOperacionTipoCambio.save(operacionTipoCambio);
	}
}
