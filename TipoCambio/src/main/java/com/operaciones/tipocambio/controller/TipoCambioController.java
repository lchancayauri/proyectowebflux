package com.operaciones.tipocambio.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.operaciones.tipocambio.model.OperacionTipoCambio;
import com.operaciones.tipocambio.model.TipoCambio;
import com.operaciones.tipocambio.security.JwtTokenProvider;
import com.operaciones.tipocambio.service.TipoCambioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/tipocambio")
public class TipoCambioController {

	private static final Logger log = LoggerFactory.getLogger(JwtTokenProvider.class);
	
	@Autowired
	TipoCambioService service;

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping
	public Flux<TipoCambio> getTiposCambio() {	
		return service.getTiposCambio();
	}

	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/{id}")
	public Mono<TipoCambio> getTipoCambio(@PathVariable Integer id) {
		return service.getTipoCambio(id);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public Mono<TipoCambio> createTipoCambio(@RequestBody TipoCambio tipoCambio) {
		return service.createTipoCambio(tipoCambio);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping
	public Mono<TipoCambio> updateCustomer(@RequestBody TipoCambio tipoCambio) {
		return service.updateTipoCambio(tipoCambio, tipoCambio.getId());

	}

	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public Mono<Void> deleteTipoCambio(@PathVariable Integer id) {
		return service.deleteTipoCambio(id);
	}
	
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	@GetMapping("/operaciones")
	public Flux<OperacionTipoCambio> getOperacionesTipoCambio() {
		return service.getOperacionsTipoCambio();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/operaciones")
	public Mono<OperacionTipoCambio> createOperacionTipoCambio(@RequestBody OperacionTipoCambio operacionTipoCambio) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		log.info("name user: "+auth.getName());
		operacionTipoCambio.setUsuarioauditoria(auth.getName());
		return service.createOperacionTipoCambio(operacionTipoCambio);
	}
}
